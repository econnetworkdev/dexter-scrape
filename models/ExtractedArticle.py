import datetime
import logging
from collections import deque
from functools import reduce

import boto3
from py2neo.types import Node, Relationship


class ExtractedArticle:
    logger = logging.getLogger("extracted_article")

    def __init__(self, article, aws_region_name="us-east-1"):
        self.aws_region_name = aws_region_name
        article.download()
        article.parse()
        self.url = article.url
        self.logger.info("Extracting Article {}".format(self.url))
        self.text = article.text
        self.title = article.title
        self.summary = article.summary
        self._entities_sentiments = self.__comprehend_API_Entities_Sentiment__()
        self.entities = self._entities_sentiments["entities"]
        self.sentiments = self._entities_sentiments["sentiments"]
        self.top_image = article.top_image
        self.movies = article.movies
        self.publish_date = article.publish_date
        self.authors = article.authors
        self.keywords = article.keywords

    def __comprehend_API_Client__(self):
        return boto3.client('comprehend', region_name=self.aws_region_name)

    def __comprehend_API_Text_Split__(self):
        return [self.text[i:i + 4500] for i in range(0, len(self.text), 4500)]

    def __comprehend_API_Entities_Sentiment__(self):
        text_split_array = self.__comprehend_API_Text_Split__()
        comprehend_client = self.__comprehend_API_Client__()
        entities = []
        sentiments = []
        for text in text_split_array:
            language_detect_response = comprehend_client.detect_dominant_language(Text=text)
            language_code = language_detect_response["Languages"][0]["LanguageCode"]
            entities_detect_response = comprehend_client.detect_entities(Text=text, LanguageCode=language_code)
            sentiment_detect_response = comprehend_client.detect_sentiment(Text=text, LanguageCode=language_code)
            sentiment_detect_response["text"] = text
            entities.append({"text": text, "entities": entities_detect_response["Entities"]})
            sentiments.append(sentiment_detect_response)

        return {"entities": entities, "sentiments": sentiments}

    def __entities_nodes_artcile_node_relation__(self, article_node, entity_obj):
        sub_graph = None
        for entity in entity_obj["entities"]:
            entity_node = Node(entity["Type"], value=entity["Text"])
            relationship = Relationship(article_node, "HAS_ENTITY", entity_node, entity_score=entity["Score"])
            if sub_graph is not None:
                sub_graph = sub_graph | relationship
            else:
                sub_graph = relationship
        return sub_graph

    def __video_nodes_from_url__(self, movie_url):
        return Node("VIDEO", url=movie_url)

    def __author_nodes_from_name_(self, author_name):
        return Node("AUTHOR", name=author_name)

    def __publish_date_str__(self):
        publish_date_str = ""
        if isinstance(self.publish_date, datetime.datetime):
            publish_date_str = self.publish_date.strftime("%Y-%m-%d %H:%M:%S")
        return publish_date_str

    def get_neo4j_sub_graph(self):
        sentiment = ""
        if len(self.sentiments) is not 0:
            sentiment = deque(map(lambda sentiment: sentiment["Sentiment"], self.sentiments)).pop()

        article_node = Node(
            "ARTICLE",
            url=self.url, text=self.text,
            title=self.title, summary=self.summary,
            publishDate=self.__publish_date_str__(), keywords=self.keywords,
            image=self.top_image,
            sentiment=sentiment
        )
        relationship = []
        if len(self.entities) is not 0:
            entities_nodes_article_node_relation = deque(map(
                lambda entity:
                self.__entities_nodes_artcile_node_relation__(article_node, entity),
                self.entities)).pop()
            relationship.append(entities_nodes_article_node_relation)
        if len(self.movies) is not 0:
            video_nodes_article_node_relation = deque(map(
                lambda movie_url:
                Relationship(article_node, "HAS_VIDEO", self.__video_nodes_from_url__(movie_url)),
                self.movies
            )).pop()
            relationship.append(video_nodes_article_node_relation)
        if len(self.authors) is not 0:
            author_nodes_article_node_relation = deque(map(
                lambda author_name:
                Relationship(article_node, "BY_AUTHOR", self.__author_nodes_from_name_(author_name)),
                self.authors
            )).pop()
            relationship.append(author_nodes_article_node_relation)

        return reduce(lambda rel1, rel2: rel1 | rel2, list(filter(lambda x: x is not None, relationship)))
