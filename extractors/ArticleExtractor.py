import logging
from collections import deque

import newspaper
from newspaper import Config
from py2neo import Graph

from models.ExtractedArticle import ExtractedArticle


class ArticleExtractor:
    logger = logging.getLogger("article_extractor")

    def __init__(self, source_url, newspaper_config={}, neo4j_graph_config={}):
        self.logger.info("Set Source URL {}".format(source_url))
        self.source_url = source_url
        self.extracted_articles = []
        self.status = "pending"
        self.newspaper_config = newspaper_config
        self.neo4j_graph_config = neo4j_graph_config

    def __neo4j_graph__(self):
        try:
            self.status = "success_neo4j_graph"
            return Graph(
                bolt=True,
                host=self.neo4j_graph_config["host"],
                bolt_port=self.neo4j_graph_config["bolt_port"],
                http_port=self.neo4j_graph_config["http_port"],
                user=self.neo4j_graph_config["user_name"],
                password=self.neo4j_graph_config["password"]
            )
        except:
            self.status = "failure_neo4j_graph"
            return None

    def extract_neo4j_persist(self):
        try:
            config = Config()
            if len(self.newspaper_config) != 0:
                if "memoize_articles" in self.newspaper_config:
                    config.memoize_articles = self.newspaper_config["memoize_articles"]
                if "verbose" in self.newspaper_config:
                    config.verbose = self.newspaper_config["verbose"]

            self.logger.info("Extracting Source URL {}".format(self.source_url))
            newspaper_source = newspaper.build(self.source_url, config=config)

            if len(newspaper_source.articles) == 0:
                raise ValueError("Expected Articles To Parse")

            self.extracted_articles = deque(map(lambda article: ExtractedArticle(article), newspaper_source.articles))
            self.logger.info("Extracted Source URL {}".format(self.source_url))
            self.status = "success_article_extract"
            self.__neo4j_persist__()
        except:
            self.status = "failure_article_extract"

    def __neo4j_persist__(self):
        neo4j_graph = self.__neo4j_graph__()
        if neo4j_graph is not None:
            try:
                extracted_articles_subgraph = []
                for extracted_article in self.extracted_articles:
                    try:
                        extracted_articles_subgraph.append(
                            extracted_article.get_neo4j_sub_graph()
                        )
                    except:
                        print("Neo4j SubGraph Failure " + extracted_article.url)

                neo4j_tx = neo4j_graph.begin()
                for neo4j_sub_graph in extracted_articles_subgraph:
                    neo4j_tx.merge(neo4j_sub_graph)

                neo4j_tx.commit()
                self.status = "success_neo4j_persist"
            except:
                self.status = "failure_neo4j_persist"
