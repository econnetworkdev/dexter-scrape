from extractors.ArticleExtractor import ArticleExtractor


def main(*source_urls):
    neo4j_graph_config = {
        "host": "ec2-52-23-228-74.compute-1.amazonaws.com",
        "bolt_port": 7687,
        "http_port": 7474,
        "user_name": "neo4j",
        "password": "i-044ddd03351b97063"
    }
    newspaper_config = {
        "memoize_articles": False,
        "verbose": True
    }
    for source in source_urls:
        artcile_extractor = ArticleExtractor(source, newspaper_config, neo4j_graph_config)
        artcile_extractor.extract_neo4j_persist()
        print(artcile_extractor.status)


if __name__ == "__main__":
    main("https://www.foxnews.com")
